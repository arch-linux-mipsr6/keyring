V=20210816

PREFIX = /usr/local

install:
	install -dm755 $(DESTDIR)$(PREFIX)/share/pacman/keyrings/
	install -m0644 archmipsr6{.gpg,-trusted,-revoked} $(DESTDIR)$(PREFIX)/share/pacman/keyrings/

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/share/pacman/keyrings/archmipsr6{.gpg,-trusted,-revoked}
	rmdir -p --ignore-fail-on-non-empty $(DESTDIR)$(PREFIX)/share/pacman/keyrings/
dist:
	git archive --format=tar --prefix=archmipsr6-keyring-$(V)/ doctor | gzip -9 > archmipsr6-keyring-$(V).tar.gz
	gpg --default-key 17832F6407942AA16F4A7AD0CC12720BD9FCE235 --detach-sign --use-agent archmipsr6-keyring-$(V).tar.gz

.PHONY: install uninstall dist
