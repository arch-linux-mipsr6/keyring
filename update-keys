#!/bin/bash

export LANG=C

TMPDIR=$(mktemp -d)
trap "rm -rf '${TMPDIR}'" EXIT

#KEYSERVER='hkp://pgp.ustc.edu.cn'
#KEYSERVER='hkp://pool.sks-keyservers.net'
#KEYSERVER='hkp://pgp.mit.edu'
#KEYSERVER='hkp://keys.gnupg.net'
#KEYSERVER='hkp://pgp.nic.ad.jp'
#KEYSERVER='hkps://keys.mailvelope.com'
#KEYSERVER='hkp://hkps.pool.sks-keyservers.net'
KEYSERVER='hkp://keyserver.ubuntu.com'

GPG="gpg --quiet --batch --no-tty --no-permission-warning --keyserver "${KEYSERVER}" --homedir ${TMPDIR}"

pushd "$(dirname "$0")" >/dev/null

$GPG --gen-key <<EOF
%echo Generating Arch Linux MIPS r6 Keyring keychain master key...
Key-Type: RSA
Key-Length: 3072
Key-Usage: sign
Name-Real: Arch Linux MIPS r6 Keyring Keychain Master Key
Name-Email: archmipsr6-keyring@localhost.localdomain
Expire-Date: 0
%no-protection
%commit
%echo Done
EOF

rm -rf master packager packager-revoked archmipsr6-trusted archmipsr6-revoked
mkdir master packager packager-revoked

while read -ra data; do
	keyid="${data[0]}"
	username="${data[@]:1}"
	${GPG} --recv-keys ${keyid} &>/dev/null
	printf 'minimize\nquit\ny\n' | \
		${GPG} --command-fd 0 --edit-key ${keyid}
	${GPG} --yes --lsign-key ${keyid} &>/dev/null
	${GPG} --armor --no-emit-version --export ${keyid} >> master/${username}.asc
	echo "${keyid}:4:" >> archmipsr6-trusted
done < master-keyids
${GPG} --import-ownertrust < archmipsr6-trusted 2>/dev/null

while read -ra data; do
	keyid="${data[0]}"
	${GPG} --recv-keys ${keyid} &>/dev/null
done < packager-keyids
while read -ra data; do
	keyid="${data[0]}"
	username="${data[@]:1}"
	printf 'clean\nquit\ny\n' | \
		${GPG} --command-fd 0 --edit-key ${keyid}
	if ! ${GPG} --list-keys --with-colons ${keyid} 2>/dev/null | grep -q '^pub:f:'; then
		echo "key is not fully trusted: ${keyid} ${username}"
	else
		${GPG} --armor --no-emit-version --export ${keyid} >> packager/${username}.asc
	fi
done < packager-keyids

while read -ra data; do
	keyid="${data[0]}"
	username="${data[1]}"
	${GPG} --recv-keys ${keyid} &>/dev/null
	printf 'clean\nquit\ny\n' | \
		${GPG} --command-fd 0 --edit-key ${keyid}
	if ! ${GPG} --list-keys --with-colons ${keyid} 2>/dev/null | grep -q '^pub:f:'; then
		${GPG} --armor --no-emit-version --export ${keyid} >> packager-revoked/${username}.asc
		echo "${keyid}" >> archmipsr6-revoked
	else
		echo "key is still fully trusted: ${keyid} ${username}"
	fi
done < packager-revoked-keyids

cat master/*.asc packager/*.asc packager-revoked/*.asc > archmipsr6.gpg

if [[ ! -f archmipsr6-revoked ]] ; then
    touch archmipsr6-revoked
fi

popd >/dev/null
